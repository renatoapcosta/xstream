package xstream;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.thoughtworks.xstream.XStream;

public class CompraTest {

	@Test
	public void deveGerarXml() {

		StringBuilder xmlExperado = new StringBuilder();
		xmlExperado.append("<compra>\n")
		 .append("  <id>15</id>\n")
		 .append("  <produtos>\n")
		 .append("    <produto codigo=\"1587\">\n")
		 .append("      <nome>geladeira</nome>\n")
		 .append("      <preco>1000.0</preco>\n")
		 .append("      <descri\u00E7\u00E3o>geladeira duas porta</descri\u00E7\u00E3o>\n")
		 .append("    </produto>\n")
		 .append("    <produto codigo=\"1588\">\n")
		 .append("      <nome>ferro de passar</nome>\n")
		 .append("      <preco>100.0</preco>\n")
		 .append("      <descri\u00E7\u00E3o>ferro com vaporizador</descri\u00E7\u00E3o>\n")
		 .append("    </produto>\n")
		 .append("  </produtos>\n")
		 .append("</compra>");

		Compra compra = compraComGeladeiraFerro();

		XStream xstream = xstreamCompraProduto();

		String xmlGerado = xstream.toXML(compra);
		assertEquals(xmlExperado.toString(), xmlGerado);

	}

	@Test
	public void deveCompararObjetoDoXml() {

		StringBuilder xmlOrigem = new StringBuilder();
		xmlOrigem.append("<compra>\n")
		 .append("  <id>15</id>\n")
		 .append("  <produtos>\n")
		 .append("    <produto codigo=\"1587\">\n")
		 .append("      <nome>geladeira</nome>\n")
		 .append("      <preco>1000.0</preco>\n")
		 .append("      <descri\u00E7\u00E3o>geladeira duas porta</descri\u00E7\u00E3o>\n")
		 .append("    </produto>\n")
		 .append("    <produto codigo=\"1588\">\n")
		 .append("      <nome>ferro de passar</nome>\n")
		 .append("      <preco>100.0</preco>\n")
		 .append("      <descri\u00E7\u00E3o>ferro com vaporizador</descri\u00E7\u00E3o>\n")
		 .append("    </produto>\n")
		 .append("  </produtos>\n")
		 .append("</compra>");

		Compra compraEsperada = compraComGeladeiraFerro();

		XStream xstream = xstreamCompraProduto();

		Compra compraDeserializada = (Compra) xstream.fromXML(xmlOrigem.toString());
		assertEquals(compraEsperada, compraDeserializada);

	}

	private XStream xstreamCompraProduto() {
		XStream xstream = new XStream();
		xstream.alias("produto", Produto.class);
		xstream.alias("compra", Compra.class);
		xstream.useAttributeFor(Produto.class, "codigo");
		xstream.aliasField("descri\u00E7\u00E3o", Produto.class, "descricao");
		return xstream;
	}

	private Compra compraComGeladeiraFerro() {
		Produto geladeira = geladeira();
		Produto ferro = ferroDePassar();
		List<Produto> produtos = new ArrayList<Produto>();
		produtos.add(geladeira);
		produtos.add(ferro);
		Compra compraEsperada = new Compra(15, produtos);
		return compraEsperada;
	}

	private Produto ferroDePassar() {
		return new Produto("ferro de passar", 100.0, "ferro com vaporizador", 1588);
	}

	private Produto geladeira() {
		return new Produto("geladeira", 1000.0, "geladeira duas porta", 1587);
	}

}
