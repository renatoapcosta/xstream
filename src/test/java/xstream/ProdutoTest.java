package xstream;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.thoughtworks.xstream.XStream;

public class ProdutoTest {

	@Test
	public void deveGerarXml() {
		StringBuilder xmlExperado = new StringBuilder();
		xmlExperado.append("<produto codigo=\"1587\">\n")
		 .append("  <nome>geladeira</nome>\n")
		 .append("  <preco>1000.0</preco>\n")
		 .append("  <descri��o>geladeira duas porta</descri��o>\n")
		 .append("</produto>");
		Produto geladeira = new Produto("geladeira", 1000.0, "geladeira duas porta", 1587);

		XStream xstream = new XStream();
		xstream.alias("produto", Produto.class);
		xstream.aliasField("descri��o", Produto.class, "descricao");
		xstream.useAttributeFor(Produto.class, "codigo");
		String xmlGerado = xstream.toXML(geladeira);

		assertEquals(xmlExperado.toString(), xmlGerado);

	}

}
